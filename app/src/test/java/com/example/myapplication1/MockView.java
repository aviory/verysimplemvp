package com.example.myapplication1;

import com.example.myapplication1.view.IView;

/**
 * Created by Кудесник on 27.06.2017.
 */

public class MockView implements IView{
    String name;

    @Override
    public String getFirstName() {
        return name;
    }

    @Override
    public String getLastName() {
        return null;
    }

    @Override
    public void setFirstName(String fname) {
        name = fname;
    }

    @Override
    public void setLastName(String lname) {

    }

    @Override
    public void clearEditTexts() {
        name = "";
    }
}
