package com.example.myapplication1;

import com.example.myapplication1.db.DataStor;
import com.example.myapplication1.presenter.presenter;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void testBtnSaveToDb() throws Exception {
        MockView v = new MockView();
        v.setFirstName("Artem");
        DataStor m = new DataStor();
        presenter p = new presenter( v, m);
        p.onClickSaveButton();
        assertEquals("Artem", m.readPerson());
    }
}