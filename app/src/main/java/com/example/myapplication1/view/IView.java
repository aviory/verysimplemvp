package com.example.myapplication1.view;

/**
 * Created by Кудесник on 27.06.2017.
 */

public interface IView {
    String getFirstName();
    String getLastName();
    void setFirstName(String fname);
    void setLastName(String lname);
    void clearEditTexts();
}
