package com.example.myapplication1.presenter;

/**
 * Created by Кудесник on 27.06.2017.
 */

public interface IPresenter {
    void onClickSaveButton();
    void onClickClearButton();
    void onClickReadButton();
    String getPerson();
}
