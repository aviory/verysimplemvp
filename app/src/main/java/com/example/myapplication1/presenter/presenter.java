package com.example.myapplication1.presenter;

import com.example.myapplication1.db.IDb;
import com.example.myapplication1.view.IView;

/**
 * Created by Кудесник on 27.06.2017.
 */

public class presenter implements IPresenter{
    private IView view;
    IDb storage;

    public presenter(IView view, IDb storage) {
        this.view = view;
        this.storage = storage;
    }

    public IView getMvpView() {
        return view;
    }
    public IDb getMvpStorage() {
        return storage;
    }


    @Override
    public void onClickSaveButton() {
        getMvpStorage().savePerson(getMvpView().getFirstName());
    }

    @Override
    public void onClickClearButton() {
        getMvpView().clearEditTexts();
        getMvpStorage().clear();
    }

    @Override
    public void onClickReadButton() {
        getMvpView().setFirstName(getMvpStorage().readPerson());
    }

    @Override
    public String getPerson() {
        return null;
    }
}
