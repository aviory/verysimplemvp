package com.example.myapplication1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.myapplication1.db.DataStor;
import com.example.myapplication1.presenter.presenter;
import com.example.myapplication1.view.IView;

public class MainActivity extends AppCompatActivity implements IView,
        View.OnClickListener {
    private presenter mPresenter;

    protected EditText edtFirstName;
    protected EditText edtLastName;
    protected Button btnSave;
    protected Button btnClear;
    protected Button btnRead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtFirstName = EditText.class.cast(findViewById(R.id.edt_first_name));
        edtLastName = EditText.class.cast(findViewById(R.id.edt_last_name));
        btnSave = Button.class.cast(findViewById(R.id.btn_save));
        btnClear = Button.class.cast(findViewById(R.id.btn_clear));
        btnRead = Button.class.cast(findViewById(R.id.btn_read));

        btnSave.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnRead.setOnClickListener(this);

        mPresenter = new presenter(this, new DataStor());
    }

    @Override
    public String toString() {
        return "MainActivity{}";
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save:
                mPresenter.onClickSaveButton();
                break;
            case R.id.btn_clear:
                mPresenter.onClickClearButton();
                break;
            case R.id.btn_read:
                mPresenter.onClickReadButton();
                break;
            default:
                try {
                    throw new Exception("No id found!");
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }

    @Override
    public String getFirstName() {
        return edtFirstName.getText().toString();
    }

    @Override
    public String getLastName() {
        return edtLastName.getText().toString();
    }

    @Override
    public void setFirstName(String fname) {
        edtFirstName.setText(fname);
    }

    @Override
    public void setLastName(String lname) {
        edtLastName.setText(lname);
    }

    @Override
    public void clearEditTexts() {
        edtFirstName.setText("");
        edtLastName.setText("");
    }
}
