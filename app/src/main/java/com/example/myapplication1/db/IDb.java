package com.example.myapplication1.db;

/**
 * Created by Кудесник on 27.06.2017.
 */

public interface IDb {
    void savePerson(String person);
    String readPerson();
    void clear();
}
