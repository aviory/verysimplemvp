package com.example.myapplication1.db;

/**
 * Created by Кудесник on 27.06.2017.
 */

public class DataStor implements IDb {
    private String data;

    @Override
    public void savePerson(String person) {
        data = person;
    }

    @Override
    public String readPerson() {
        return data;
    }

    @Override
    public void clear() {
        data = "";
    }
}
